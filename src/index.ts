import * as express from 'express';
import * as expressLogger from 'express-logging';
import { container } from './inversify.config'; 
import { TYPES } from './TYPES';
import { IApplication } from "./interfaces/IApplication";
import { IEnv } from './interfaces/IEnv';
import { IOHalter } from './utils/IOHalter';
import { AlertingWorker } from './workers/AlertingWorker';

const app = express();

app.set('port', container.get<IEnv>(TYPES.Env).nodePort);
app.use(expressLogger(container.get<any>(TYPES.Logger)));

const importController = container.get<IApplication>(TYPES.ImportController);
const statsController = container.get<IApplication>(TYPES.StatsController);

const alertWorker = container.get<AlertingWorker>(TYPES.AlertingWorker);

const ioHalter = container.get<IOHalter>(TYPES.IOHalter);

app.use(importController.app);
app.use(statsController.app);

app.get('/status', (request:express.Request, response:express.Response) => {
    response.send('OK');
});

Promise.all(ioHalter.getAllPromises()).then(() => {
    app.listen(app.get('port'), () => {
        console.log(`Application is listening on PORT ${app.get('port')}`)
    });

    // Initiating the worker
    alertWorker.initatePolling();
}).catch((e) => {
    console.error('Something went wrong while booting up');
    console.error(e);
});

