import { injectable, inject } from 'inversify';
import { Db, Collection } from 'mongodb';
import { Stream } from 'stream';
import * as Rx from 'rxjs/Rx';
import * as RxNode from 'rx-node';

import { TYPES } from '../TYPES';
import { CsvEntity } from '../models/CsvEntity';
import { IConf } from '../interfaces/IConf';
import { MongoEntityForCreation } from '../models/MongoEntityForCreation';
import { MongoDbClient } from '../clients/MongoClient';

@injectable()
export class ImportService {
    private mongoDbClient:MongoDbClient;

    constructor(@inject(TYPES.MongoDbClient) mongoDbClient:MongoDbClient) {
        this.mongoDbClient = mongoDbClient;
    }

    uploadCsvStream = (csvStream:Stream):Rx.Observable<any> => {
        return RxNode.fromStream(csvStream)
            .map(item => new MongoEntityForCreation(item))
            .flatMap((mongoEntity:MongoEntityForCreation) => this.mongoDbClient.createAlertDocument(mongoEntity));
    }
}