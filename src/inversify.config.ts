import { Container } from "inversify";
import * as inversify from "inversify";
import "reflect-metadata";

import { TYPES } from './TYPES';
import { IApplication } from "./interfaces/IApplication";
import { IEnv } from './interfaces/IEnv';
import { IOHalter } from './utils/IOHalter';
import { IConf } from './interfaces/IConf';
import { ImportController } from './controllers/ImportController';
import { StatsController } from './controllers/StatsController';
import { ImportService } from './services/ImportService';
import { MongoConnection } from './db/MongoConnection';
import { AlertingWorker } from './workers/AlertingWorker';
import { AlertingService } from './services/AlertingService';
import { MongoDbClient } from './clients/MongoClient';
import { CommClient } from './clients/CommClient';
import * as logger from 'logops';
import { env } from './env';
import { conf } from './conf';

const container = new Container();

container.bind<IEnv>(TYPES.Env).toConstantValue(env);
container.bind<IConf>(TYPES.Conf).toConstantValue(conf);
container.bind<any>(TYPES.Logger).toConstantValue(logger);
container.bind<ImportService>(TYPES.ImportService).to(ImportService);
container.bind<IOHalter>(TYPES.IOHalter).to(IOHalter);
container.bind<MongoConnection>(TYPES.MongoConnection).to(MongoConnection).inSingletonScope();
container.bind<IApplication>(TYPES.ImportController).to(ImportController).inSingletonScope();
container.bind<IApplication>(TYPES.StatsController).to(StatsController).inSingletonScope();
container.bind<AlertingWorker>(TYPES.AlertingWorker).to(AlertingWorker);
container.bind<AlertingService>(TYPES.AlertingService).to(AlertingService);
container.bind<MongoDbClient>(TYPES.MongoDbClient).to(MongoDbClient);
container.bind<CommClient>(TYPES.CommClient).to(CommClient);

export {container};