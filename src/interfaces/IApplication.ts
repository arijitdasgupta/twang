import * as express from 'express';

export interface IApplication {
    app: express.Application;
}