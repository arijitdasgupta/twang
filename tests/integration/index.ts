import * as assert from 'assert';
import { describe, it, beforeEach, afterEach, before, after } from 'mocha';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import { promisify } from 'util';
import { MongoClient } from 'mongodb';

const childProcess = require('child_process');

const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;

import { env } from '../../src/env';
import { map } from 'rxjs/operators/map';
import { readFileSync } from 'fs';

const simpleCommApp = (cb = (x:any) => null, resFn = (...x) => {}) => {// Setting up a simple comm service
    const app = express();
    app.use(bodyParser.json());
    app.post('/messages', (req, res) => {
        cb(req);
        res.send(resFn());
    });
    return app;
}

/**
 * The tests mock the commservice, as well as checks the MongoDB collections for number of docs etc.
 */
describe('twang integration tests', () => {
    let db;
    let dbName;
    let nodeProc;
    let commApp;
    let commServer;

    before(() => {
        chai.use(chaiHttp);
    });

    const setupMongoDbAndAppclication = async (dbName) => {
        db = await MongoClient.connect(`${env.mongoHost}`);

        // Inserts a dummy collection, probably not needed
        await db.db(dbName).collection('test').insert({t: 'test'});

        console.log('Spawning the node application');
        nodeProc = childProcess.spawn(`node`, ['./build/src/index.js'], {
            env: {
                ...process.env,
                COMMSERVICE_HOST: 'http://localhost:3333',
                MONGO_DBNAME: dbName,
                PORT: "9090"
            }
        });

        // logs out applications' stderr
        nodeProc.stderr.on('data', (d) => {
            console.log(d.toString());
        });

        return new Promise(resolve => setTimeout(resolve, 2000)); // Wait till the actually application boots up
    }

    // Before each test, a new DB is created, and a the application is fired up
    beforeEach(async () => {
        dbName = `integration-test-db-${(new Date()).getTime()}`;

        return setupMongoDbAndAppclication(dbName);
    });

    // After each test, the DB is dropped, the application is killed and the dummy comm server closed
    afterEach(done => {
        console.log('Cleaning up...');
        nodeProc.kill();
        commServer.close();

        // Deleting the database
        db.db(dbName).dropDatabase().then(() => done());
    });

    describe('/uploadCsv', () => {
        it('Should add correct number of docs docs to the DB', async () => {
            const csvFileText = readFileSync('./tests/stuff/customers1.csv').toString();

            commApp = simpleCommApp();

            await new Promise(resolve => {
                commServer = commApp.listen(3333, resolve)
            });

            await chai.request('http://localhost:9090')
                .post('/uploadCsv')
                .send(csvFileText)
                .then(d => {
                    db.db(dbName).collection('alerts').count({}).then(n => {
                        expect(n).to.equal(2);
                    });
                });

            return Promise.resolve();
        });

        it('After upload should call the comm service endpoint with proper data', async () => {
            const csvFileText = readFileSync('./tests/stuff/customers1.csv').toString();

            let reqBody;

            const reqHandle = (req) => {
                reqBody = req.body;
            };

            commApp = simpleCommApp(reqHandle);

            await new Promise(resolve => {
                commServer = commApp.listen(3333, resolve)
            });

            await chai.request('http://localhost:9090')
                .post('/uploadCsv')
                .send(csvFileText);

            const proms = [];

            proms.push(new Promise(resolve => {
                setTimeout(() => {
                    expect(reqBody.email).to.equal('gomgom@gogo.com');
                    expect(reqBody.text).to.equal('Hi GomGom, your invoice about $XD is due.')

                    reqBody = {};
                    resolve();
                }, 2000);
            }));
                
            proms.push(new Promise(resolve => {
                setTimeout(() => {
                    expect(reqBody.email).to.equal('vdaybell0@seattletimes.com');
                    expect(reqBody.text).to.equal('Hi Vincenty, your invoice about $1.99 is due.')

                    reqBody = {};
                    resolve();
                }, 3000);
            }));

            proms.push(new Promise(resolve => {
                setTimeout(() => {
                    expect(reqBody.email).to.equal('gomgom@gogo.com');
                    expect(reqBody.text).to.equal('Hi GomGom, your invoice about $XD is due.')

                    reqBody = {};
                    resolve();
                }, 4000);
            }));

            proms.push(new Promise(resolve => {
                setTimeout(() => {
                    expect(reqBody.email).to.equal('vdaybell0@seattletimes.com');
                    expect(reqBody.text).to.equal('Hi Vincenty, your invoice about $1.99 is due.');

                    reqBody = {};
                    resolve();
                }, 7000);
            }));

            return Promise.all(proms);
        });

        it('After upload the app should respect the `paid` flag', async () => {
            const csvFileText = readFileSync('./tests/stuff/customers2.csv').toString();

            let reqBody;
            let resObj = {};

            const reqHandle = (req) => {
                reqBody = req.body;
            };

            const resGen = () => resObj;

            commApp = simpleCommApp(reqHandle, resGen);

            await new Promise(resolve => {
                commServer = commApp.listen(3333, resolve)
            });

            await chai.request('http://localhost:9090')
                .post('/uploadCsv')
                .send(csvFileText);

            const proms = [];

            resObj = { paid: true }; // Setting the paid flag
                
            proms.push(new Promise(resolve => {
                setTimeout(() => {
                    expect(reqBody.email).to.equal('vdaybell0@seattletimes.com');
                    expect(reqBody.text).to.equal('Hi Vincenty, your invoice about $1.99 is due.')

                    reqBody = {email: 't', text: 't'};
                    resolve();
                }, 3000);
            }));

            proms.push(new Promise(resolve => {
                setTimeout(() => {
                    // Didn't get any new request, since the values remained the same...
                    expect(reqBody.email).to.equal('t');
                    expect(reqBody.text).to.equal('t');

                    reqBody = {};
                    resolve();
                }, 7000);
            }));

            await Promise.all(proms);

            const count = await db.db(dbName).collection('alerts').count({paid: true});

            expect(count).to.equal(1);

            return Promise.resolve();
        });
    });
});
    