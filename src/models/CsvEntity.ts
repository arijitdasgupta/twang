export class CsvEntity {
    email: string | null;
    text: string | null;
    offsets: number[];

    constructor(obj:any) {
        this.email = obj.email ? obj.email : null;
        this.text = obj.text ? obj.text : null;

        const offsets = obj.schedule ? obj.schedule : null;

        if (offsets) {
            this.offsets = offsets.split('-').map(off => off.replace('s', '')).map(i => parseInt(i));
        } else {
            this.offsets = [];
        }
    }
}