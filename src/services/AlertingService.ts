import { injectable, inject } from 'inversify';
import * as Rx from 'rxjs/Rx';

import { TYPES } from '../TYPES';
import { MongoDbClient } from '../clients/MongoClient';
import { MongoEntityForReading } from '../models/MongoEntityForReading';
import { CommClient } from '../clients/CommClient';

interface IAlertAndTime {
    doc:MongoEntityForReading;
    timestamp:number;
}

interface IAlertTimeResp extends IAlertAndTime {
    commData: any;
}

@injectable()
export class AlertingService {
    commClient: CommClient;
    mongoDbClient: MongoDbClient;

    constructor(@inject(TYPES.MongoDbClient) mongoDbClient:MongoDbClient,
                @inject(TYPES.CommClient) commClient:CommClient) {
        this.commClient = commClient;
        this.mongoDbClient = mongoDbClient;
    }

    /**
     * Takes and stream and returns a new stream of alert documents that's stored in the db
     */
    public getOutstandingAlerts = (ticker:Rx.Observable<any>):Rx.Observable<IAlertAndTime> => {
        return ticker.flatMap(() => {
            const timestamp = (new Date()).getTime();

            return this.mongoDbClient.lockAndGetOutStandingAlerts(timestamp).map(doc => ({
                doc, timestamp
            }));
        });
    }

    /**
     * Takes a stream of alert docs, and sends alerts to according to the docs to the comm service
     */
    public sendAlert = (rxDoc:Rx.Observable<IAlertAndTime>):Rx.Observable<IAlertTimeResp> => {
        return rxDoc.flatMap(alertObject => {
            return this.commClient.sendAlert(alertObject.doc).then(commData => ({
                ...alertObject,
                commData
            }));
        });
    }

    /**
     * Takes a stream of alert docs containing the original document and alert response, and sets the paids flag
     * as needed
     */
    public setPaidFlagAptly = (alertObjectRx:Rx.Observable<IAlertTimeResp>):Rx.Observable<IAlertTimeResp> => {
        return alertObjectRx.flatMap(alertObject => {
            const newObs = Rx.Observable.of(alertObject)
            if (!!alertObject.commData.paid) {
                return newObs.flatMap(alertObject => this.mongoDbClient.setPaid(alertObject.doc._id, true).then(() => alertObject));
            } else {
                return newObs;
            }
        });
    }

    /**
     * Takes a stream of alert docs containing the origianl document, timestampd, alert response etc. and returns
     * a new observable the new timestamp array appropriately
     */
    public setTimestamps = (alertObjectRx:Rx.Observable<IAlertTimeResp>):Rx.Observable<IAlertTimeResp> => {
        return alertObjectRx.map(alertObject => {
            alertObject.doc = alertObject.doc.filterTimeToSend(alertObject.timestamp);
            return alertObject;
        });
    }

    /**
     * Takes a stream of IAlertTimeResp and flattens to saving the timestamps
     */
    public saveTimestamps = (alertObjectRx:Rx.Observable<IAlertTimeResp>):Rx.Observable<IAlertTimeResp> => {
        return alertObjectRx.flatMap(alertObject => 
            this.mongoDbClient.setTimesToAlert(alertObject.doc._id, alertObject.doc.timestosend).then(() => alertObject)
        );
    }

    /**
     * Takes a stream of IAlertTimeResp and unlocks the appropriate document
     */
    public unlockAlertDoc = (alertObjectRx:Rx.Observable<IAlertTimeResp>):Rx.Observable<IAlertTimeResp> => {
        return alertObjectRx.flatMap(alertObject => {
            return this.mongoDbClient.unlockAlert(alertObject.doc._id).then(() => alertObject);
        });
    }
}