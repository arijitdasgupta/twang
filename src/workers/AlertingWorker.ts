import { injectable, inject } from 'inversify';
import * as Rx from 'rxjs/Rx';

import { TYPES } from '../TYPES';
import { IConf } from '../interfaces/IConf';
import { MongoDbClient } from '../clients/MongoClient';
import { MongoEntityForReading } from '../models/MongoEntityForReading';
import { CommClient } from '../clients/CommClient';
import { AlertingService } from '../services/AlertingService';

@injectable()
export class AlertingWorker {
    alertingService:AlertingService;
    mongoDbClient: MongoDbClient;
    logger: any;

    constructor(@inject(TYPES.AlertingService) alertingService:AlertingService,
                @inject(TYPES.MongoDbClient) mongoDbClient:MongoDbClient,
                @inject(TYPES.Logger) logger:any) {
        this.alertingService = alertingService;
        this.mongoDbClient = mongoDbClient;
        this.logger = logger;
    }

    initatePolling = () => {
        const ticker = Rx.Observable.interval(1000);

        const outstandingRx = this.alertingService.getOutstandingAlerts(ticker);
        const alertedRx = this.alertingService.sendAlert(outstandingRx);
        const setPaidFlag = this.alertingService.setPaidFlagAptly(alertedRx);
        const setTimestamps = this.alertingService.setTimestamps(setPaidFlag);
        const saveTimestamps = this.alertingService.saveTimestamps(setTimestamps);
        const unlockAlerts = this.alertingService.unlockAlertDoc(saveTimestamps);

        unlockAlerts.subscribe(d => {
            this.logger.info('Sent email to', d.doc.email);

            this.mongoDbClient.appendToLog({
                email: d.doc.email,
                text: d.doc.text,
                timestamp: d.timestamp
            });
        });
    }
}