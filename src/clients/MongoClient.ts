import { injectable, inject } from 'inversify';
import { Collection, Db, OrderedBulkOperation } from 'mongodb';
import * as Rx from 'rxjs';

import { TYPES } from '../TYPES';
import { IConf } from '../interfaces/IConf';
import { MongoConnection } from '../db/MongoConnection';
import { MongoEntityForCreation } from '../models/MongoEntityForCreation';
import { MongoEntityForReading } from '../models/MongoEntityForReading';
import { timestamp } from 'rxjs/operator/timestamp';

@injectable()
export class MongoDbClient {
    private alertColl: Collection;
    private logColl: Collection;

    constructor(@inject(TYPES.MongoConnection) mongoConnection:MongoConnection, @inject(TYPES.Conf) conf:IConf) {
        mongoConnection.dbPromise.then(db => {
            this.alertColl = db.collection(conf.mongoAlertCollection);
            this.logColl = db.collection(conf.logsCollection);
        });    
    }

    private getQueryForOutstandingAlerts = (timestamp:number) => {
        return {
            paid: false,
            locked: false,
            timestosend: { $lt: timestamp }
        };
    }

    createAlertDocument = (entityToCreate:MongoEntityForCreation):Promise<any> => {
        const mongoDoc = entityToCreate.toNewMongoDocument((new Date()).getTime());
        return this.alertColl.insert(mongoDoc);
    }

    /**
     * Goes through all the documents, and sets locked flag true for all the valid alert docs, and sends out a stream/observable
     * of those alert documents
     */
    lockAndGetOutStandingAlerts = (timestamp:number):Rx.Observable<MongoEntityForReading> => {
        return Rx.Observable.create((observer) => {
            const recursive = () => {
                setTimeout(() => {
                    this.alertColl.findOneAndUpdate(this.getQueryForOutstandingAlerts(timestamp), {$set: {locked: true}}).then((doc) => {
                        if (doc.value) {
                            observer.next(new MongoEntityForReading(doc.value));
                            recursive();
                        } else {
                            observer.complete();
                        }
                    }).catch(e => observer.error(e));
                }, 0);
            };

            recursive();
        })
    }

    setTimesToAlert = (id:string, timestosend: number[]):Promise<any> => {
        return this.alertColl.update({_id: id}, {$set: {timestosend}});
    }

    setPaid = (docId:string, paidFlag: true):Promise<any> => {
        return this.alertColl.update({_id: docId}, {$set: {paid: paidFlag}});
    }

    unlockAlert = (docId:string):Promise<any> => {
        return this.alertColl.update({_id: docId}, { $set: { locked: false }});
    }

    appendToLog = (doc:any):Promise<any> => {
        return this.logColl.insert(doc);
    }

    getLogCount = ():Promise<number> => {
        return this.logColl.count({});
    }
}