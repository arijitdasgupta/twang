import { MongoEntityForCreation } from './MongoEntityForCreation';

export class MongoEntityForReading extends MongoEntityForCreation {
    _id: string;
    timestosend: number[];

    constructor(obj:any) {
        super(obj, {locked: !!obj.locked, paid: !!obj.paid});

        this._id = obj._id;
        this.timestosend = obj.timestosend;
    }

    filterTimeToSend = (timestamp:number):MongoEntityForReading => {
        this.timestosend = this.timestosend.filter(time => time > timestamp);

        return this;
    }

    toUpdateMongoDocument():any {
        const mongoDoc = this.toMongoDocument();
        mongoDoc._id = this._id;
        mongoDoc.timestosend = this.timestosend;

        return mongoDoc;
    }

    toCommServicePayload():any {
        return {
            email: this.email,
            text: this.text
        };
    }
}