import { IConf } from "./interfaces/IConf";

export const conf:IConf = {
    mongoAlertCollection: 'alerts',
    logsCollection: 'logs'
}