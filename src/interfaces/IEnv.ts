export interface IEnv {
    nodePort: number;
    mongoHost: string;
    mongoDbName: string;
    commServiceHost: string;
}