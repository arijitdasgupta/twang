Twang
=====

A simple REST enabled scheduling system
---------------------------------------

This system uses MongoDB and stores timestamps, and the scheduling is based on polling. The user `POST`s a proprely arranged CSV file to `/uploadCsv` endpoint. Once the upload is starts, the ticker mechanism checks if there are valid alerts to send and sends them to the commservice accordingly. The application is designed to be HTTP oriented, and also takes history storage into account. All the alerts that are ever sent or to be sent is stored in the database, for troubleshooting etc.

Also it exposes a `/stats` endpoint. The endpoint returns some statistics about the system's operation.

### Technicals

The system makes use of a rudimentary locking mechanism, where the alert documents don't get re-read re-posted until the lock is release. Which means during restart of the service, the resources in process will never get consumed again. 

However, when comparing polling approach vs. timeout/queue approach, during restart/crashes less number of scheduled alerts will be lost.

### Running it

You would need to have `docker`, `docker-compose`, and `node` & `npm` installed in order to run it. 

For development, you would also need a mock commserivce. `(commservice.mac/commservice.linux)` running locally. You would also need some sample CSV that has the scheme for `sample.csv` in the applicaiton source directory to test out the scheduling.

### To get the MongoDB up and running
```bash
docker-compose up
```

### To get the application running in dev mode
```bash
npm install
npm run watch
```

### To run in production mode
```bash
npm install
npm run build
npm start
```

### To run the test
```bash
npm install
npm run test
```

### Sample CSV post command to trigger the scheduling
```bash
curl -X POST http://localhost:6700/uploadCsv -d <path-to-csv>
```

### Environment variables

 - `MONGO_DBNAME` Default: `twang-db` [MongoDB database name]
 - `MONGO_HOST` Default: `mongodb://localhost:27017` [MongoDB host]
 - `PORT` Default: `5000` [Port to run the application on]
 - `COMMSERVICE_HOST` Default: `http://localhost:9090` [Commservice host]

### Testing

 - As of now the code is unit testable, however, the application isn't unit tested. Since it's time based action firing, it has integration tests testing timings of the alerting that it's meant to do. 

 - The integration tests spin up a instance of the application on port 9090, and mock the commservice on port 3333 and makes sure the application is sending alerts properly, and writing data to the MongoDB properly.

### NOTES
 - CSV file upload size is 2MBs.

### Caveats
 - No unit tests.
 - `/stats` endpoint is very rudimentary.
 - No schema check, validation for CSV upload. Breakage in the CSV structure will result in erratic application behaviour.
 - *If the comm service fails, the system can't handle it*. The ticking mechanism stops as of now. But can be easily fixed.
 - The time format is CSV only support format of `123s-12s-123s` or `123-13-24`. The time formatter only understands times as seconds.
 - Re-uploading CSV file will trigger the alerts again. No uniqueness check for the alert entries.

### TODOS
 - Add error handling at all places and graceful handling of external service failures.
 - Add validation to the upload endpoint
 - Add unit tests
 - Fix the models
 - Add better time formatter



 