import { CsvEntity } from './CsvEntity';

interface IMetaProps {
    locked: boolean;
    paid: boolean;
}

export class MongoEntityForCreation extends CsvEntity {
    locked: boolean;
    paid: boolean;

    constructor(obj, metaProps:IMetaProps = {locked: false, paid: false}) {
        super(obj);

        this.locked = metaProps.locked;
        this.paid = metaProps.paid;
    }


    protected toMongoDocument = ():any => {
        return {
            email: this.email,
            text: this.text,
            locked: this.locked,
            paid: this.paid,
        };
    }

    toNewMongoDocument = (currentTimestamp:number):any => {
        const mongoDoc = this.toMongoDocument();
        mongoDoc.timestosend = this.offsets.map(offset => currentTimestamp + offset * 1000);
        return mongoDoc;
    }
}