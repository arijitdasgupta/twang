import * as express from 'express';
import { injectable, inject } from 'inversify';
import * as bodyParser from 'body-parser';
import * as stream from 'stream';
import * as csv from 'csv-parser';

import { IApplication } from "../interfaces/IApplication";
import { TYPES } from '../TYPES';
import { ImportService } from '../services/ImportService';

@injectable()
export class ImportController implements IApplication {
    public app: express.Application;

    constructor(@inject(TYPES.ImportService) private importService:ImportService) {
        this.app = express();

        this.app.use(bodyParser.raw({
            inflate: true,
            limit: '2048kb',
            type: '*/*'
        }));

        this.app.post('/uploadCsv', this.uploadFile);
    }

    uploadFile:express.RequestHandler = (request, response) => {
        const bodyStream = new stream.PassThrough();
        bodyStream.end(request.body);

        const csvStream = bodyStream.pipe(csv());

        this.importService.uploadCsvStream(csvStream).subscribe(
            () => null, 
            e => {
                console.error(e);
                response.status(500).send();
            },
            () => response.send('OK')
        );
    }
}