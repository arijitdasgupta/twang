import * as express from 'express';
import { injectable, inject } from 'inversify';

import { IApplication } from "../interfaces/IApplication";
import { TYPES } from '../TYPES';
import { MongoDbClient } from '../clients/MongoClient';

@injectable()
export class StatsController implements IApplication {
    public app: express.Application;
    private mongoDbClient:MongoDbClient;

    constructor(@inject(TYPES.MongoDbClient) mongoDbClient:MongoDbClient) {
        this.mongoDbClient = mongoDbClient;

        this.app = express();

        this.app.get('/stats', this.handleStats);
    }

    handleStats:express.RequestHandler = async (request, response) => {
        response.send({
            numberOfAlertsSent: await this.mongoDbClient.getLogCount()
        });
    }
}