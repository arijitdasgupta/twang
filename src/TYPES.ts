export const TYPES = {
    ImportController: Symbol(),
    StatsController: Symbol(),
    Env: Symbol(),
    MongoConnection: Symbol(),
    MongoDbClient: Symbol(),
    IOHalter: Symbol(),
    ImportService: Symbol(),
    Conf: Symbol(),
    AlertingWorker: Symbol(),
    AlertingService: Symbol(),
    CommClient: Symbol(),
    Logger: Symbol()
}