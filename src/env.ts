import { IEnv } from './interfaces/IEnv';

export const env:IEnv = {
    mongoDbName: process.env.MONGO_DBNAME || 'twang-db',
    mongoHost: process.env.MONGO_HOST || 'mongodb://localhost:27017',
    nodePort:  !isNaN(parseInt(process.env.PORT, 10)) ? parseInt(process.env.PORT, 10) : 5000,
    commServiceHost: process.env.COMMSERVICE_HOST || 'http://localhost:9090'
};