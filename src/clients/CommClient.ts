import { injectable, inject } from 'inversify';
import axios, { AxiosInstance } from 'axios';

import { TYPES } from '../TYPES';
import { IEnv } from '../interfaces/IEnv';
import { MongoEntityForReading } from '../models/MongoEntityForReading';

@injectable()
export class CommClient {
    private axiosClient: AxiosInstance;
    constructor(@inject(TYPES.Env) env:IEnv) {
        this.axiosClient = axios.create({
            baseURL: env.commServiceHost
        });
    }

    sendAlert = (alert:MongoEntityForReading):Promise<any> => {
        return this.axiosClient.post('/messages', alert.toCommServicePayload())
            .then(resp => resp.data);
    }
}