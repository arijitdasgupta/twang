import { injectable, inject } from 'inversify';
import { MongoClient, Db } from 'mongodb';

import { TYPES } from '../TYPES';
import { IEnv } from '../interfaces/IEnv';
import { IOHalter } from '../utils/IOHalter';

@injectable()
export class MongoConnection {
    dbPromise: Promise<Db>;

    constructor(@inject(TYPES.Env) env:IEnv, @inject(TYPES.IOHalter) ioHalter:IOHalter) {
        this.dbPromise = MongoClient.connect(env.mongoHost).then(client => {
            return client.db(env.mongoDbName);
        });
        ioHalter.addPromise(this.dbPromise);
    }
}